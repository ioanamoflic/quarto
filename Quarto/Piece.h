#pragma once

#include <cstdint>
#include <iostream>

class Piece
{
public:
	enum class Colour : uint8_t
	{
		WHITE, BROWN
	};

	enum class Shape : uint8_t
	{
		SQUARE, CIRCLE
	};

	enum class Height : uint8_t
	{
		SHORT, TALL
	};

	enum class Body : uint8_t
	{
		HOLLOW, FULL
	};

	Piece(Colour, Shape, Height, Body);

	Colour GetColour() const noexcept;
	Shape GetShape() const noexcept;
	Height GetHeight() const noexcept;
	Body GetBody() const noexcept;

	friend std::ostream& operator<<(std::ostream& out, const Piece& other);

private:
	Colour m_colour : 1;
	Shape m_shape : 1;
	Height m_height : 1;
	Body m_body : 1;

};


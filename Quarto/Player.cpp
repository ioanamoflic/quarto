#include "Player.h"

Player::Player(std::string name)
	:playerName(name)
{

}

std::ostream& operator<<(std::ostream& output, const Player& player)
{
	output << player.playerName;
	return output;
}

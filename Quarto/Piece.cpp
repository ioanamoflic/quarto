#pragma once
#include "Piece.h"

Piece::Piece(Colour colour, Shape shape, Height height, Body body) :
	m_colour(colour), m_shape(shape), m_height(height), m_body(body)
{
	static_assert(sizeof(*this) == 1, "Size greater than 1");
}

Piece::Colour Piece::GetColour() const noexcept
{
	return m_colour;
}

Piece::Shape Piece::GetShape() const noexcept
{
	return m_shape;
}

Piece::Height Piece::GetHeight() const noexcept
{
	return m_height;
}

Piece::Body Piece::GetBody() const noexcept
{
	return m_body;
}

std::ostream& operator<<(std::ostream& out, const Piece& other)
{
	out << static_cast<int>(other.m_body)
		<< static_cast<int>(other.m_colour)
		<< static_cast<int>(other.m_height)
		<< static_cast<int>(other.m_shape);
	return out;
}

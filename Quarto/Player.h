#pragma once
#include <string>

class Player
{
public:
	Player(std::string name);
	friend std::ostream& operator <<(std::ostream& output, const Player&);

private:
	std::string playerName;
};


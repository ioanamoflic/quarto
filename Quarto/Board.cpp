#include "Board.h"

std::optional<Piece>& Board::operator[](const Position& position)
{
	const auto& [rowIndex, columnIndex] = position;
	return m_board[rowIndex * m_length + columnIndex];
}

const std::optional<Piece>& Board::operator[](const Position& position) const
{
	const auto& [rowIndex, columnIndex] = position;
	return m_board[rowIndex * m_length + columnIndex];
}

const char emptyBoardCell[] = "____";

std::ostream& operator<<(std::ostream& out, const Board& myBoard)
{
	Board::Position position;
	auto& [indexRow, indexColumn] = position;
	for (indexRow = 0; indexRow < Board::m_length; ++indexRow)
	{
		for (indexColumn = 0; indexColumn < Board::m_width; ++indexColumn)
			if (auto& optPiece = myBoard[position]; optPiece)
				out << *optPiece << " ";
			else
				out << emptyBoardCell << " ";
		out << "\n";
	}
	return out;
}
#pragma once
#include "Piece.h"
#include <array>
#include <optional>

class Board
{
public:
	Board() = default;
	using Position = std::pair<uint8_t, uint8_t>;
	std::optional<Piece>& operator[](const Position& position);
	const std::optional<Piece>& operator[](const Position& position) const;
	friend std::ostream& operator<<(std::ostream& out, const Board&);

private:
	static const int m_length = 4;
	static const int m_width = 4;
	static const int m_size = m_length * m_width;

	std::array<std::optional<Piece>, m_size> m_board;
};


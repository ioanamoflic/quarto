#include "UnusedPieces.h"

UnusedPieces::UnusedPieces()
{
	Initialize();
}

Piece UnusedPieces::choosePiece(const std::string& key)
{
	auto extracted = m_unusedPieces.extract(key);
	if (extracted)
		return std::move(extracted.mapped());
	else throw "Piece not found.";
}

void UnusedPieces::Initialize()
{

	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::CIRCLE, Piece::Height::TALL, Piece::Body::HOLLOW));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::CIRCLE, Piece::Height::TALL, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::CIRCLE, Piece::Height::SHORT, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::CIRCLE, Piece::Height::SHORT, Piece::Body::HOLLOW));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::SQUARE, Piece::Height::TALL, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::SQUARE, Piece::Height::SHORT,Piece::Body::HOLLOW));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::SQUARE, Piece::Height::SHORT, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::WHITE, Piece::Shape::SQUARE, Piece::Height::TALL, Piece::Body::HOLLOW));

	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::CIRCLE, Piece::Height::TALL, Piece::Body::HOLLOW));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::CIRCLE, Piece::Height::TALL, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::CIRCLE, Piece::Height::SHORT, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::CIRCLE, Piece::Height::SHORT, Piece::Body::HOLLOW));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::SQUARE, Piece::Height::TALL, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::SQUARE, Piece::Height::SHORT, Piece::Body::HOLLOW));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::SQUARE, Piece::Height::SHORT, Piece::Body::FULL));
	this->Emplace(Piece(Piece::Colour::BROWN, Piece::Shape::SQUARE, Piece::Height::TALL, Piece::Body::HOLLOW));

}

void UnusedPieces::Emplace(const Piece& piece)
{
	std::stringstream ss;
	ss << piece;
	m_unusedPieces.insert(std::make_pair(ss.str(), piece));
}

std::ostream& operator<<(std::ostream& output, const UnusedPieces& up)
{
	for (auto unusedPiece : up.m_unusedPieces)
		output << unusedPiece.first <<" ";
	output << "\n";

	return output;
}

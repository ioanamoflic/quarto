#include <iostream>
#include "Piece.h"
#include "Board.h"
#include "UnusedPieces.h"


int main()
{
	UnusedPieces unusedPiece;
	Board board;
	std::cout << "Empty board:\n";
	std::cout << board;

	std::cout << "Initial pieces:\n";
	std::cout << unusedPiece;

	board[{0,0}] = unusedPiece.choosePiece("1101");

	std::cout << "Not empty board:\n";
	std::cout << board;

	std::cout << "Current pieces:\n";
	std::cout << unusedPiece;

}